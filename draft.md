# Graffito: A markup language for diagrams

## API

### Entities

Entities are defined in the format `<symbol>: <title>`. `a: Alpha` will produce:

    xxxxxxxxx
    x Alpha x
    xxxxxxxxx

### Connectors

Connectors are defined with one or more dash characters between entities.

Spaces, if any, between an entity symbol and a connector are ignored.

Connector modifiers may appear at the end(s) of a connector. Defined modifiers:

| Symbol | Position         | Description |
| -----: | :--------------: | :--------------------------------------- |
|    `<` | beginning or end | Left arrow                               |
|    `>` | beginning or end | Right arrow                              |
|    `|` | beginning        | Branch from previously defined connector |
|    `|` | middle           | Branch here (for label positioning)      |

Note that the directionality ("left" or "right") of modifiers refers to the
definition string, not the direction of the connector in the output.

Text appearing in the connector will be displayed as a label.

## Examples

### Simplest Relationship

Input

    a: Alpha
    b: Bravo
    
    a-b

Output

    xxxxxxxxx       xxxxxxxxx
    x Alpha x ----- x Bravo x
    xxxxxxxxx       xxxxxxxxx

### Connector with label

Input

    a: Alpha
    b: Bravo
    c: Charlie

    a -Yes-> b
    a -No-> c

Output

    xxxxxxxxx  Yes  xxxxxxxxx
    x Alpha x ----> x Bravo x
    xxxxxxxxx       xxxxxxxxx
        |
        | No
        |
        V
    xxxxxxxxxxx
    x Charlie x
    xxxxxxxxxxx

### Split connectors

Input

a: Alpha
b: Bravo
c: Charlie

    a -> b
      |-> c

Ouput

    xxxxxxxxx       xxxxxxxxx
    x Alpha x --\-> x Bravo x
    xxxxxxxxx   |   xxxxxxxxx
                |
                |   xxxxxxxxxxx
                \-> x Charlie x
                    xxxxxxxxxxx

### Split connectors with labels

Input

a: Alpha
b: Bravo
c: Charlie

    a -foo-|-bar-> b
           |-baz-> c

Ouput

    xxxxxxxxx       xxxxxxxxx
    x Alpha x --\-> x Bravo x
    xxxxxxxxx   |   xxxxxxxxx
                |
                |   xxxxxxxxxxx
                \-> x Charlie x
                    xxxxxxxxxxx
